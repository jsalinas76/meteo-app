import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardPrincipalComponent } from './dashboard-principal/dashboard-principal.component';
import { FormsModule } from '@angular/forms';
import { DashboardProvinciaComponent } from './dashboard-provincia/dashboard-provincia.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardPrincipalComponent,
    DashboardProvinciaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
