import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-principal',
  templateUrl: './dashboard-principal.component.html',
  styleUrls: ['./dashboard-principal.component.css']
})
export class DashboardPrincipalComponent implements OnInit {

  titulo: string;
  provincias: Array<any>;
  ciudades: Array<any>;
  today: Array<string>;
  tomorrow: Array<string>;
  defaultSelected: string;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.titulo = '';
    this.provincias = new Array<any>();
    this.ciudades = new Array<any>();
    this.today = new Array<string>();
    this.tomorrow = new Array<string>();
    this.defaultSelected = '';
   }

   ngOnInit(): void {
    this.getProvincias();
  }

  getProvincias(): void {
    this.http.get<any>('https://www.el-tiempo.net/api/json/v2/home')
      .subscribe(res => {
        console.log(res);
        this.titulo = res.title;
        this.provincias = res.provincias;
        this.defaultSelected = this.provincias[0].CODPROV;
        this.ciudades = res.ciudades;
        this.today = res.today.p;
        this.tomorrow = res.tomorrow.p;
      }, err => {
        console.error(err);
      });
  }

  goToProvinciaInfo(form: NgForm) {
    if (form.valid) {
      this.router.navigate(['/provincia', form.value.provincia]);
    }
  }
}
