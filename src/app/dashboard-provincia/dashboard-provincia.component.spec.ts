import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardProvinciaComponent } from './dashboard-provincia.component';

describe('DashboardProvinciaComponent', () => {
  let component: DashboardProvinciaComponent;
  let fixture: ComponentFixture<DashboardProvinciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardProvinciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardProvinciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
