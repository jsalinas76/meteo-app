import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-dashboard-provincia',
  templateUrl: './dashboard-provincia.component.html',
  styleUrls: ['./dashboard-provincia.component.css']
})
export class DashboardProvinciaComponent implements OnInit {

  codigoProvincia: string = '';
  municipiosDestacados = new Array<any>();
  previsionHoy : string = '';
  previsionManana : string = '';
  nombreProvincia: string = '';

  constructor(private rutaActiva: ActivatedRoute, private httpClient: HttpClient) {}

  ngOnInit(): void {
    // Recuperando el parámetro
    this.rutaActiva.params.subscribe((params: Params) => {
      this.codigoProvincia = params.codigo;
      this.httpClient.get<any>(`https://www.el-tiempo.net/api/json/v2/provincias/${this.codigoProvincia}`).subscribe(response => {
        this.municipiosDestacados = response.ciudades;
        this.previsionHoy = response.today.p;
        this.previsionManana = response.tomorrow.p;
        this.nombreProvincia = response.provincia.NOMBRE_PROVINCIA;
      });
    });
  }
}
