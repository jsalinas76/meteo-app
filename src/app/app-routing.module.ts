import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardPrincipalComponent } from './dashboard-principal/dashboard-principal.component';
import { DashboardProvinciaComponent } from './dashboard-provincia/dashboard-provincia.component';

const routes: Routes = [
  {path:'', component: DashboardPrincipalComponent},
  {path:'provincia/:codigo', component: DashboardProvinciaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
